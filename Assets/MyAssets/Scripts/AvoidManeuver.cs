﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidManeuver : MonoBehaviour
{

    public int radiusDetectionPredator = 40;
    private Predator predator;
    private bool predatorDetected;
    private Boid boidComponent;
    // Start is called before the first frame update
    void Start()
    {
        predator = FindObjectOfType<Predator>();
        predatorDetected = false;
        boidComponent = this.GetComponent<Boid>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 directionFishPredator = predator.transform.position - this.transform.position;

        if (Vector3.Distance(this.transform.position, predator.transform.position) <= radiusDetectionPredator && Vector3.Angle(this.transform.forward, directionFishPredator) < 150){
            predatorDetected = true;
            Debug.DrawRay(this.transform.position, directionFishPredator, Color.red);
            boidComponent.controlOnOutsideService = true;
            boidComponent.cohesionWeight = 3f;
            boidComponent.separationWeight = 0.5f;

            boidComponent.whereToGo = this.transform.forward * 2;
            boidComponent.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(predator.transform.forward), Time.deltaTime * 5);
            //boidComponent.settings.perceptionRadius = 4;
        }
        else
        {
            predatorDetected = false;
            boidComponent.controlOnOutsideService = false;
            //if(boidComponent.settings.perceptionRadius == 4)
                //boidComponent.settings.perceptionRadius = defaultCohesionRadius;

        }

    }

}


enum PredatorState
{
    predator_Presence,
    predator_Chase
}