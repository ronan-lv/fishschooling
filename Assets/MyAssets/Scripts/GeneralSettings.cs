﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/* We finally didn't use this class */
[CreateAssetMenu]
public class GeneralSettings : ScriptableObject
{

    public float thresholdNearestNeighborDistance_NNDt = 4.0f;
    public float speed_S = 0.15f;
    public float perceptionLength_L = 10.0f;
    public float tresholdForce_Ft = 5;
    public float skitterDistance_Ds = 0.02f;
    //is the value im interested in ?
    public float thresholdAvoidDistance_Dat = 2;
    public float thresholdFear_Fet = 1;
    public float minimumApproachDistance_Mad = 1;
    public float thresholdNumberOfFish_Nt = 150;
    public float explosionTime_Te = 0.4f;
    public float timeToExcecuteAvoidManeuver_Tam = 2;
    public float startleTime_Ts = 0.7f;
    public float leaderDistance_Ld = 2.0f;
    public float herdDistance_Dg = 0.9f;
    public float herdRadiusRange_Hr = 0.5f;
    public float tresholdBallDistance_Det = 2.5f;
    public float explosionTime2_Te2 = 0.09f;
    public float predatorRippleForceInPC_Fir = 5;
    public float predatorRippleForceInPA_Fir = 10;
    //predator presence ADDED BY MYSELF
    public float thresholdForceforPp = 50;
    //predator chase
    public float thresholdForceForPC_Ft = 10;
    //predator attack
    public float tresholdForceForPA_Ft = 15;
    public float tresholdCollision_Ct = 0.05f;
    public float startleTime2_Ts2 = 0.01f;
    public float leaderDistanceRange_Ldr = 1;
    public float constantC1_C1 = 2;
    public Vector2 constantC2_C2 = new Vector2(-355.06f, 43.3f);
    public Vector2 constantC3_C3 = new Vector2(-350, 45);
   
}