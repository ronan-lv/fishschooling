﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetationGenerator : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject alguePrefab;
    public int spacing = 5;
    public int randomness = 5;

    private Terrain terrain;

    void Start()
    {
        terrain = GameObject.FindObjectOfType<Terrain>();

        float terrainCoordinateMaxX = terrain.transform.position.x +  terrain.terrainData.size.x;
        float terrainCoordinateMaxZ = terrain.transform.position.z +  terrain.terrainData.size.z;

        int randomX, randomZ;

        for(int x = (int) terrain.transform.position.x; x < terrainCoordinateMaxX ; x += spacing)
        {
            for(int z = (int) terrain.transform.position.z; z < terrainCoordinateMaxZ; z += spacing)
            {
                randomX = x + Random.Range(-randomness, randomness);
                randomZ = z + Random.Range(-randomness, randomness);
                alguePrefab.transform.position = new Vector3(randomX, terrain.SampleHeight(new Vector3(randomX, 0, randomZ)), randomZ);
                alguePrefab.transform.eulerAngles = new Vector3(alguePrefab.transform.eulerAngles.x, alguePrefab.transform.eulerAngles.y, Random.Range(0, 360));
                Instantiate(alguePrefab);
            }
        }
    }

    // Update is called once per frames
    void Update()
    {
        
    }
}
