﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {

    [HideInInspector]
    public BoidSettings settings;

    // State
    [HideInInspector]
    public Vector3 position;
    [HideInInspector]
    public Vector3 forward;
    [HideInInspector]
    public Vector3 velocity;

    // To update:
    [HideInInspector]
    public Vector3 acceleration;
    [HideInInspector]
    public Vector3 avgFlockHeading;
    [HideInInspector]
    public Vector3 avgAvoidanceHeading;
    [HideInInspector]
    public Vector3 centreOfFlockmates;
    [HideInInspector]
    public int numPerceivedFlockmates;

    [HideInInspector]
    public float cohesionWeight;
    [HideInInspector]
    public float separationWeight;
    [HideInInspector]
    public Vector3 whereToGo;

    [HideInInspector]
    public bool controlOnOutsideService = false;




    // Cached
    Transform cachedTransform;

    void Awake () {
        cachedTransform = transform; 
    }

    public void Initialize (BoidSettings settings) {
        this.settings = settings;

        position = cachedTransform.position;
        forward = cachedTransform.forward;

        float startSpeed = (settings.minSpeed + settings.maxSpeed) / 2;
        velocity = transform.forward * startSpeed;
        
    }


    public void UpdateBoid () {
        Vector3 acceleration = Vector3.zero;

        if (!controlOnOutsideService)
        {
            cohesionWeight = settings.cohesionWeight;
            separationWeight = settings.seperateWeight;
        }

        /*
        Debug.Log("alignement weight : " + settings.alignWeight);
        Debug.Log("cohesion weight : " + cohesionWeight);
        Debug.Log("separation weight :" + separationWeight);
        */



        if (numPerceivedFlockmates != 0) {
            
            centreOfFlockmates /= numPerceivedFlockmates;

            Vector3 offsetToFlockmatesCentre = (centreOfFlockmates - position);

            Vector3 alignmentForce = SteerTowards (avgFlockHeading) * settings.alignWeight;
            Vector3 cohesionForce = SteerTowards (offsetToFlockmatesCentre) * cohesionWeight;
            Vector3 seperationForce = SteerTowards (avgAvoidanceHeading) * separationWeight;

            acceleration += alignmentForce;
            acceleration += cohesionForce;
            acceleration += seperationForce;
        }

        if (IsHeadingForCollision ()) {
            Vector3 collisionAvoidDir = ObstacleRays ();
            Vector3 collisionAvoidForce = SteerTowards (collisionAvoidDir) * settings.avoidCollisionWeight;
            acceleration += collisionAvoidForce;
        }

            
        if (!controlOnOutsideService)
        {

            velocity += acceleration * Time.deltaTime;
            float speed = velocity.magnitude;
            Vector3 direction = velocity / speed;
            speed = Mathf.Clamp(speed, settings.minSpeed, settings.maxSpeed);
            velocity = direction * speed;
            cachedTransform.position += velocity * Time.deltaTime;
            cachedTransform.forward = direction;
            position = cachedTransform.position;
            forward = direction;
        }
        else
        {
            acceleration += SteerTowards(whereToGo) * 10;

            velocity += acceleration * Time.deltaTime;
            float speed = velocity.magnitude;
            Vector3 direction = velocity / speed;
            speed = settings.maxSpeed;
            velocity = direction * speed;
            cachedTransform.position += velocity * Time.deltaTime;
            position = cachedTransform.position;
        }

    }

    bool IsHeadingForCollision () {
        RaycastHit hit;
        if (Physics.SphereCast (position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDst, settings.obstacleMask)) {
            return true;
        } else { }
        return false;
    }

    Vector3 ObstacleRays () {
        Vector3[] rayDirections = BoidHelper.directions;

        for (int i = 0; i < rayDirections.Length; i++) {
            Vector3 dir = cachedTransform.TransformDirection (rayDirections[i]);
            Ray ray = new Ray (position, dir);
            if (!Physics.SphereCast (ray, settings.boundsRadius, settings.collisionAvoidDst, settings.obstacleMask)) {
                return dir;
            }
        }

        return forward;
    }

    Vector3 SteerTowards(Vector3 vector) {
        Vector3 v = vector.normalized * settings.maxSpeed - velocity;
        return Vector3.ClampMagnitude (v, settings.maxSteerForce);
    }

}