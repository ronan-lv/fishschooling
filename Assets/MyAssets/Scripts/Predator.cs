﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predator : MonoBehaviour
{
    private Boid[] arrayBoids; 

    public int radiusDetectionFishes =  25;
    public float speed = 0.2f;

    private List<Boid> listDetectedFishes;


    private Vector3 preyFishSchoolCentroid; // Gc

    // Start is called before the first frame update
    void Start()
    {
        arrayBoids = FindObjectsOfType<Boid>();
        listDetectedFishes = new List<Boid>();
    }

    // Update is called once per frame
    void Update()
    {
        //Update the list of detected fish
        UpdateFishDetected();
        //If they are so fishes detected
        if(listDetectedFishes.Count != 0)
        {

            //Find the most isolate fish
            foreach (Boid fish in listDetectedFishes)
                preyFishSchoolCentroid += fish.transform.position;

            preyFishSchoolCentroid /= listDetectedFishes.Count;

            //Find the most isolated fish
            Boid isolatedFish = listDetectedFishes[0];
            foreach(Boid fish in listDetectedFishes)
            {
                if (Vector3.Distance(fish.transform.position, preyFishSchoolCentroid) < Vector3.Distance(isolatedFish.transform.position, preyFishSchoolCentroid))
                    isolatedFish = fish;
            }

            //Debug.DrawLine(this.transform.position, isolatedFish.transform.position, Color.white);
        }

        this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed);

    }

    //Visualize the radius around the predator
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(255, 0, 0, 0.3f);
        //Gizmos.DrawSphere(this.transform.position, radiusDetectionFishes);
    }


    private void UpdateFishDetected()
    {
        foreach (Boid boid in arrayBoids)
        {
            if (Vector3.Distance(this.transform.position, boid.transform.position) <= radiusDetectionFishes)
            {
                if (!listDetectedFishes.Contains(boid))               
                    listDetectedFishes.Add(boid);               
            }
            else
            {
                if (listDetectedFishes.Contains(boid))
                    listDetectedFishes.Remove(boid);
            }
        }

    }
}
