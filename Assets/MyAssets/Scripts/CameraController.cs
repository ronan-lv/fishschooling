﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{



float normalMoveSpeed = 10.0f; //regular speed
float shiftAdd = 250.0f; //multiplied by how long shift is held.  Basically running
float shiftMoveSpeed = 100.0f; //Maximum speed when holdin gshift
float camSensibility = 2f; //How sensitive it with mouse
private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
private float totalRun = 1.0f;

void Update()
{

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


        if (Input.mousePosition.x >= 0 && Input.mousePosition.x <= Screen.width && Input.mousePosition.y >= 0 && Input.mousePosition.y <= Screen.height)
        {
            lastMouse = new Vector3(Input.GetAxis("Mouse Y") * -camSensibility, Input.GetAxis("Mouse X") * camSensibility, 0);
            lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
            transform.eulerAngles = lastMouse;
            lastMouse = Input.mousePosition;

        }


    //Keyboard commands
    Vector3 p = GetBaseInput();
    if (Input.GetKey(KeyCode.LeftShift))
    {
        totalRun += Time.deltaTime;
        p = p * totalRun * shiftAdd;
        p.x = Mathf.Clamp(p.x, -shiftMoveSpeed, shiftMoveSpeed);
        p.y = Mathf.Clamp(p.y, -shiftMoveSpeed, shiftMoveSpeed);
        p.z = Mathf.Clamp(p.z, -shiftMoveSpeed, shiftMoveSpeed);
    }
    else
    {
        totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
        p = p * normalMoveSpeed;
    }

    p = p * Time.deltaTime;
    Vector3 newPosition = transform.position;
    if (Input.GetKey(KeyCode.Space))
    { //If player wants to move on X and Z axis only
        transform.Translate(p);
        newPosition.x = transform.position.x;
        newPosition.z = transform.position.z;
        transform.position = newPosition;
    }
    else
    {
        transform.Translate(p);
    }

}

private Vector3 GetBaseInput()
{ //returns the basic values, if it's 0 than it's not active.
    Vector3 p_Velocity = new Vector3();
    //Forward
    if (Input.GetKey(KeyCode.Z))
    {
        p_Velocity += new Vector3(0, 0, 1);
    }
    //backward
    if (Input.GetKey(KeyCode.S))
    {
        p_Velocity += new Vector3(0, 0, -1);
    }
    //Left
    if (Input.GetKey(KeyCode.Q))
    {
        p_Velocity += new Vector3(-1, 0, 0);
    }
    //Right
    if (Input.GetKey(KeyCode.D))
    {
        p_Velocity += new Vector3(1, 0, 0);
    }
    return p_Velocity;
}
}
