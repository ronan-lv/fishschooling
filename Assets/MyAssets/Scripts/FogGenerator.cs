﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogGenerator : MonoBehaviour
{
    public bool fogActivation = true;
    public float fogDensity = 0.003f;
    // Start is called before the first frame update
    void Start()
    {
        RenderSettings.fogColor = Camera.main.backgroundColor;
        RenderSettings.fogDensity = fogDensity;
        RenderSettings.fog = fogActivation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
