﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public enum GizmoType { Never, SelectedOnly, Always }

    public Boid prefab;
    public float spawnRadius = 10;
    public int spawnCount = 10;
    public Color colour;
    public GizmoType showSpawnRegion;

    void Awake () {
        for (int i = 0; i < spawnCount; i++) {

            Vector3 position = GenerateARandomVectorInASphere(transform.position, spawnRadius);
            Boid boid = Instantiate(prefab);
            boid.transform.position = position;
            boid.transform.forward = Vector3.forward;
        }
    }

    private void OnDrawGizmos () {
        if (showSpawnRegion == GizmoType.Always) {
            DrawGizmos ();
        }
    }

    void OnDrawGizmosSelected () {
        if (showSpawnRegion == GizmoType.SelectedOnly) {
            DrawGizmos ();
        }
    }

    void DrawGizmos () {
        Gizmos.color = new Color (colour.r, colour.g, colour.b, 0.3f);
        Gizmos.DrawSphere(transform.position, spawnRadius);
    }

        public Vector3 GenerateARandomVectorInASphere(Vector3 sphereMiddle, float radiusSphere)
    {


        Vector3 coordinates = sphereMiddle + Random.onUnitSphere * radiusSphere;

        Vector3 directionPoint = coordinates - sphereMiddle;

        while (Vector3.Angle(Vector3.forward, directionPoint) > 30)
        {
            coordinates = sphereMiddle + Random.onUnitSphere * radiusSphere;
            directionPoint = coordinates - sphereMiddle;
        }

        coordinates = new Vector3(coordinates.x, coordinates.y, Random.Range(coordinates.z - 3, coordinates.z + 3));

        return coordinates;

    }

}