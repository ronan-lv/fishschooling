Here you can find our project for the Collective behaviour class of 2020 Winter semester.
In this project we implemented a fish schooling behavior in addition to a prey-predator model, with emphasis on the visual appearance of the simulation.
For this purpose we used the work of Sahithi Podila in 'Animating predator and prey fish interactions'. 
Our goal was to create a creative and realistic environment, which we achieved by means of terrain, assets, caustic effect and random generation. 
Furthermore we wanted a realistic and credible fish schooling behavior.

We finally obtained a fluid rendering and a rather nice environment (although we are not able to evaluate this notion objectively). 
Fish schooling also gives convincing results. However the implementation of the prey-predator model was reduced to a compact-avoid manuever which, 
although functional, does not give very visual results. It is possible to walk through the created scene, so we hope you will enjoy the ride.

To test the simulation, simply load the 'Main_scene' and then click on 'Play'. (Assets->MyAssets->Scenes->Main Scene)

Enjoy! 
